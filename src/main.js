import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import EasySlider from 'vue-easy-slider'
import VueCarousel from 'vue-carousel';
import Vuex from 'vuex'
import VueAnimate from 'vue-animate-scroll'
import { store } from './store'
import moment from 'moment';

Vue.use(VueAnimate);
Vue.use(Vuex);
Vue.use(VueCarousel);
Vue.use(EasySlider);
Vue.use(VueRouter);

const router = new VueRouter({
  routes: [
    { path: '/about', component: () => import('@/pages/about.vue') },
    { path: '/residence', component: () => import('@/pages/residence.vue') },
    { path: '/contact', component: () => import('@/pages/contact.vue') },
    { path: '/compare', component: () => import('@/pages/compare.vue') },
    { path: '/citizenship/:id', component: () => import('@/pages/citizenship/_id.vue') },
    { path: '/citizenship', component: () => import('@/pages/citizenship/index.vue') },
    { path: '/blog', component: () => import('@/pages/blog/index.vue') },
    { path: '/blog/:id', component: () => import('@/pages/blog/_id.vue') },
    { path: '/', component: () => import('@/pages/index.vue') },
  ],
  scrollBehavior () {
    return { x: 0, y: 0 }
  }
});

Vue.config.productionTip = false;

Vue.filter('formatDate', function(value) {
    if (value) {
        return moment(String(value)).format('MM/DD/YYYY hh:mm');
    }
});


new Vue({
  render: h => h(App),
  router,
  store,
}).$mount('#app')
